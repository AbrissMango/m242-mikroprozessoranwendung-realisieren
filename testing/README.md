# Anforderungen

## Inhaltsverszeichnis
- [Anforderungen](#anforderungen)
  - [Inhaltsverszeichnis](#inhaltsverszeichnis)
    - [Testprotokoll](#testprotokoll)

### Testprotokoll
| # | Soll | Ist | OK / NOK |
|---|------|-----|----------|
| 1 | WiFi Verbindung wird automatisch wieder aufgebaut | WiFi Verbindung wird automatisch wieder aufgebaut | OK |
| 2 | MySQL Verbindung wird automatisch wieder aufgebaut | MySQL Verbindung wird automatisch wieder aufgebaut | OK |
| 3 | Bei Knopfdruck wird eine andere Sensor-Data auf dem LCD Display angezeigt | Bei Knopfdruck wird eine andere Sensor-Data auf dem LCD Display angezeigt | OK |
| 4 | Verbindungsunterbruch zu MySQL oder WiFi wird auf dem LCD Display angezeigt | Verbindungsunterbruch zu MySQL oder WiFi wird auf dem LCD Display angezeigt | OK |
