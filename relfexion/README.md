# Anforderungen

## Inhaltsverszeichnis
- [Anforderungen](#anforderungen)
  - [Inhaltsverszeichnis](#inhaltsverszeichnis)
    - [Reflexion Andreas Giger](#reflexion-andreas-giger)
    - [Reflexion Janik Bolli](#reflexion-janik-bolli)

### Reflexion Andreas Giger
Hier kommt text...

### Reflexion Janik Bolli
Das Modul 242 war in meinen Augen ein gutes Modul. Wir hatten die Chance viel alleine zu machen und auch die Theorie selbst zu erarbeiten.
Ebenso war es nicht einfach trocken indem wir alles Theoretisch machen mussten, sonder wir durften selbst ein Hands-On Projekt durchführen.
Rückblickend auf unser Projekt muss ich sagen, dass wir den Rahmen genau getroffen haben. Wir hatten nicht überdurchschnittlich zuviel zu tun, jedoch auch nicht zu wenig.
Unser Projekt konnten wir schön aufteilen, so dass ich den RaspberryPi teil übernommen habe und Anderas Giger konnte den Arduino teil mit dem Coden übernehmen. So waren wir am effizientesten. 
Die Planung der Aufgaben gins so auf, dass wir den grössten Teil während den Lektionen machen konnte. Das einzige wir Zuhause machen mussten, war die Dokumentation und auch diese ging nicht sonderlich lang.