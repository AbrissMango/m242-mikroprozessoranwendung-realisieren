# Analyse

## Inhaltsverszeichnis
- [Analyse](#analyse)
  - [Inhaltsverszeichnis](#inhaltsverszeichnis)
    - [Funktionale/ nicht Funktionale Anforderungen](#funktionale-nicht-funktionale-anforderungen)
    - [Blockschaltbild](#blockschaltbild)
    - [Signalbeschrieb](#signalbeschrieb)

### Funktionale/ nicht Funktionale Anforderungen 
Die Anforderungen für unser Projekt haben wir in einer Tabelle festgehalten. In dieser Tabelle ist ebenfalls direkt ersichtlich, ob diese Funktion funktional oder nicht funktional ist.<br>
<table>
<thead>
  <tr>
    <th colspan="2">Name</th>
    <th>Beschreibung und Zielsetznung</th>
    <th>Verbindlichkeit</th>
    <th>Funktional/ nicht Funktional</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td>Req-1</td>
    <td>WiFi Verbindung</td>
    <td>Der Arduino kann mit dem Modul "WiFiNINA" über WLAN, mit dem Handyhotspot welcher die SSID "QuetschPhone" hat verbinden</td>
    <td>zwingend</td>
    <td>Funktional</td>
  </tr>
  <tr>
    <td>Req-2</td>
    <td>Sensor</td>
    <td>Der ENV-Shield kann die mit den Sensoren die Installiert sind, die Temperatur, die Helligkeit und die Feuchtigkeit lesen. <br>Die Temperatur kann er von -40°C bis +120°C lesen, dies mit einer Genauigkeit von ± 0,5 °C im Bereich von 15 bis +40 °C.<br>Die Helligkeit kann er von 440 nm bis zu 800 nm sehen. Der Feuchtigkeitsbereich reicht von ± 3,5 % rH (relative humidity), bei 20 (°C) bis +80 % rH</td>
    <td>zwingend</td>
    <td>Funktional</td>
  </tr>
  <tr>
    <td>Req-3</td>
    <td>LCD Display</td>
    <td>Die gelesenen Daten vom ENV-Shield werden auf dem "Grove LCD RGB Backlight" angezeigt werden.<br>Temperatur zeigt er im Format xx.xx°C an, Feuchtigkeit zeigt er xx.xx% und Helligkeit zeigt er in xx.xx lx an</td>
    <td>optional</td>
    <td>Funktional</td>
  </tr>
  <tr>
    <td>Req-4</td>
    <td>Button</td>
    <td>Bei einem klick auf den Knopf welcher mit einem PullUp-Widerstand angeschlossen ist, wird der Wert gewechselt der auf dem LCD Display angezeigt wird. So soll zwischen Temperatur, Helligkeit &amp; Feuchtigkeit hin und her geschalten werden.<br>Die werte werden jeweils einzeln auf einer Linie angezeigt</td>
    <td>optional</td> 
    <td>Funktional</td>
  </tr>
  <tr>
    <td>Req-5</td>
    <td>mySQL Datenbank</td>
    <td>Auf dem RaspberryPi läuft eine MariaDB, mit einer Datenbank namens "Arduino". Diese hat drei Tabellen (temperature, humidity, light) welche wieder zwei Spalten (wert (y), timestamp) (x)) hat. So wird in jeder Tabelle jeweils ein Wert gespeichert.<br>Der Wert wird als FLOAT gespeichert und kommt vom ENV-Shield, der Timestamp kommt von Arduino welcher diesen von einem NTP-Server bezieht, dieser Wert wird als INT gespeichert.</td>
    <td>zwingend</td>
    <td>Funktional</td>
  </tr>
  <tr>
    <td>Req-6</td>
    <td>mySQL Senden</td>
    <td>Der Arduino kann die Daten von den Sensoren an die jeweils korrekte Tabelle senden. Ebenfalls schickt er den aktuelle Timestamp mit, damit wir diesen auf der Website verwenden können.</td>
    <td>zwingend</td>
    <td>Funktional</td>
  </tr>
  <tr>
    <td>Req-7</td>
    <td>WebServer</td>
    <td>Der Webserver auf dem RaspberryPi kann Daten aus der Datenbank auslesen und diese in einem Liniendiagramm anzeigen. Dieses Diagramm machen wir mit canvasJS.<br>Für jeden Wert haben wir eine einzelnes Diagramm.</td>
    <td>zwingend</td>
    <td>Funktional</td>
  </tr>
  <tr>
    <td>Req-8</td>
    <td>Bouncing Button</td>
    <td>Der Knopf hat das Problem, dass dieser Bounced. Das heisst, dass ein Klick 2-3 Registriert wird. Dies soll mit einem Delay verhindert werden.</td>
    <td>optional</td>
    <td>nicht Funktional</td>
  </tr>
</tbody>
</table>

### Blockschaltbild      
![Blockschaltbild](/analyse/images/Blockschlatbild.png "Projektschaltbild")*Blockschaltbild von Sensor2Data. Quelle: Privat* <br> 

###  Signalbeschrieb
**Stromquelle**
Die Stromquelle liefert über Mirco-USB *5V* an den Arduino MKR 1010. <br>

**LCD Display**
Das LCD Display verwendet eine Spannung von *5V*. Dadurch braucht es auch eine Erdung, wesshalb das Display auch an *GND* angeschlossen und somit geerdet. Die digitalen Ports *D11* & *D12* werden dazu verwendet die Daten auf dem Display anzuzeigen.<br>

**Button**
Unser Knopf ist nur an *D0* und *GND* angeschlossen. Dies kommt liegt daran, dass wir uns entschieden haben den *PullUp-Widerstand* im Code selbst einzubauen.<br>

**MKR ENV Shield**
Die Pins *D11* und *D12* werden auch hier beim Sensor verwendet. Diese beiden Pins sind dafür zuständig die Daten der Sensoren an das Arduino weiterzugeben. 