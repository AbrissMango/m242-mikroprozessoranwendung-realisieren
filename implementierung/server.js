const fs = require("fs");
const MySQL = require("mysql");

// create mysql connection string
const con = MySQL.createConnection({
  host: "localhost",
  user: "arduino",
  password: "Password1",
  database: "arduino"
});

// connect to mysql database
con.connect((err) => {
  if(err) {
    log("Fehler beim Verbinden mit der Datenbank:\n" + err);
  } else {
    // create http server
    require("http").createServer(async (req, res) => {
      switch(req.url) {
        case "/update":
          // select sensor data from all 3 tables
          con.query("SELECT * FROM humidity ORDER BY `x` DESC LIMIT 40", [], (err_humidity, res_humidity) => {
            if(err_humidity) {
              res.writeHead(500);
              res.end();
            } else {
              con.query("SELECT * FROM light ORDER BY `x` DESC LIMIT 40", [], (err_light, res_light) => {
                if(err_light) {
                  res.writeHead(500);
                  res.end();
                } else {
                  con.query("SELECT * FROM temperature ORDER BY `x` DESC LIMIT 40", [], (err_temperature, res_temperature) => {
                    if(err_temperature) {
                      res.writeHead(500);
                      res.end();
                    } else {
                      // format data
                      jsonArrayToString(res_humidity, (res_humidity) => {
                        jsonArrayToString(res_light, (res_light) => {
                          jsonArrayToString(res_temperature, (res_temperature) => {
                            // send data to client
                            res.writeHead(200, {"Content-Type": "text/plain"});
                            res.end(res_humidity + "\n" + res_light + "\n" + res_temperature);
                          });
                        });
                      });
                    }
                  });
                }
              });
            }
          });
          break;
        default:
          // stream html file
          res.writeHead(200, {"Content-Type": "text/html", "Content-Length": fs.statSync("./chart.html").size});
          fs.createReadStream("./chart.html").pipe(res);
      }
    }).listen(80);
  }
});

function jsonArrayToString(jsonArray, cb) {
  // 0 rows selected from database?
  if(jsonArray.length == 0) {
    cb("");
  } else {
    let arrayString = "";
    // loop through selected rows
    jsonArray.forEach((json, i) => {
      // prepare timestamp for javascript
      json["x"] = json["x"] * 1000;

      // combine all rows into one string
      arrayString += "|" + JSON.stringify(json);

      // return string when done with all rows
      if(i == (jsonArray.length - 1)) {
        cb(arrayString.substring(1));
      }
    });
  }
}