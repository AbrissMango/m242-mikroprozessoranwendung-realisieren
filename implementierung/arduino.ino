#include <Arduino_MKRENV.h>
#include <SPI.h>
#include <WiFiNINA.h> 
#include <WiFiUdp.h>
#include <NTPClient.h>
#include <Wire.h>
#include <rgb_lcd.h>
#define USING_WIFININA_GENERIC false
#define USING_WIFININA true
#include <MySQL_Generic.h>

// Load WiFi credentials from secrets file
#include "arduino_secrets.h"
char ssid[] = SECRET_SSID;
char pass[] = SECRET_PASS;

// Define LCD screen
rgb_lcd lcd;

// define buttonstate variable
// this tells us which sensor data we
// should display on the lcd screen
int buttonState = 0;

// Initialize MySQL connection
MySQL_Connection conn((Client *)&client);

// Initialize network time
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP);

void setup() {
  // initialize the serial port
  Serial.begin(9600);

  // is MKR ENV shield connected?
  if(!ENV.begin()) {
    Serial.println("Failed to initialize MKR ENV shield!");
  } else {
    // initialize LCD display and clear it
    lcd.begin(16, 2);
    lcd.display();
    lcd.setCursor(0, 0);
    lcd.clear();

    // initialize button (pin 0) and define interrupt function
    pinMode(0, INPUT_PULLUP);
    attachInterrupt(digitalPinToInterrupt(0), buttonPress, FALLING);
    
    // check if the WiFi module works
    if (WiFi.status() == WL_NO_SHIELD) {
      Serial.println("WiFi shield not present");
    } else {
      Serial.print("Attempting to connect to SSID: ");
      Serial.println(ssid);
  
      // connect to WPA/WPA2 network
      WiFi.begin(ssid, pass);
  
      // wait 10 seconds for connection
      delay(10000);
      if(WiFi.status() == WL_CONNECTED) {
        Serial.println("WiFi connected!");
        Serial.println();
        // connect to mysql database
        if(conn.connectNonBlocking(IPAddress(172, 20, 10, 4), 3306, "arduino", "Password1") != RESULT_FAIL) {
          Serial.println("MySQL connected!");
          Serial.println();
        } else {
          lcd.clear();
          lcd.print("MySQL lost");
          Serial.println("MySQL connection failed");
          Serial.println();
        }
        
        // start the ntp client
        timeClient.begin();
      }
    }
  }
} 

void loop() {
  // check WiFi connection:
  if(WiFi.status() != WL_CONNECTED) {
    lcd.clear();
    lcd.print("WiFi lost");
    Serial.print("WiFi connection lost, trying to reconnect to SSID: ");
    Serial.println(ssid);

    // reconnect to WPA/WPA2 network
    WiFi.begin(ssid, pass);

    // wait 10 seconds for connection
    delay(10000);
    if(WiFi.status() == WL_CONNECTED) {
      lcd.clear();
      Serial.println("WiFi connected!");
    }
    Serial.println();
  } else {
    // still connected
    
    // update ntp time
    timeClient.update();
    
    // print each of the sensor values
    Serial.print("Temperatur = ");
    Serial.print(ENV.readTemperature());
    Serial.println(" °C");
  
    Serial.print("Feuchtigkeit = ");
    Serial.print(ENV.readHumidity());
    Serial.println(" %");
  
    Serial.print("Helligkeit = ");
    Serial.print(ENV.readIlluminance());
    Serial.println(" lx");

    // is mysql still connected?
    if(conn.connected()) {
      // insert sensor data into database
      MySQL_Query(&conn).execute(("INSERT INTO arduino.temperature (`y`,`x`) VALUES (" + String(ENV.readTemperature()) + ", " + String(timeClient.getEpochTime()) + ")").c_str());
      MySQL_Query(&conn).execute(("INSERT INTO arduino.humidity (`y`,`x`) VALUES (" + String(ENV.readHumidity()) + ", " + String(timeClient.getEpochTime()) + ")").c_str());
      MySQL_Query(&conn).execute(("INSERT INTO arduino.light (`y`,`x`) VALUES (" + String(ENV.readIlluminance()) + ", " + String(timeClient.getEpochTime()) + ")").c_str());
    } else {
      lcd.clear();
      lcd.print("MySQL lost");
      Serial.println("MySQL connection lost, trying to reconnect");
      // reconnectg to database
      if(conn.connectNonBlocking(IPAddress(172, 20, 10, 4), 3306, "arduino", "Password1") != RESULT_FAIL) {
        Serial.println("MySQL connected!"); 
      }
    }
  }

  // print on lcd display depending on current buttonstate value
  if(buttonState == 0) {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Temperatur");
    lcd.setCursor(0, 1);
    lcd.print(String(ENV.readTemperature()) + " " + (char)223 + "C");
  } else if(buttonState == 1) {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Feuchtigkeit");
    lcd.setCursor(0, 1);
    lcd.print(String(ENV.readHumidity()) + " %");
  } else {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Helligkeit");
    lcd.setCursor(0, 1);
    lcd.print(String(ENV.readIlluminance()) + " lx");
  }

  // print an empty line and wait
  Serial.println();
  delay(60000);
}

void buttonPress() {
  // switch buttonstate value
  if(buttonState > 1) {
    buttonState = 0;
  } else {
    buttonState++;
  }

  // print on lcd display depending on current buttonstate
  if(buttonState == 0) {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Temperatur");
    lcd.setCursor(0, 1);
    lcd.print(String(ENV.readTemperature()) + " " + (char)223 + "C");
  } else if(buttonState == 1) {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Feuchtigkeit");
    lcd.setCursor(0, 1);
    lcd.print(String(ENV.readHumidity()) + " %");
  } else {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Helligkeit");
    lcd.setCursor(0, 1);
    lcd.print(String(ENV.readIlluminance()) + " lx");
  }
}
