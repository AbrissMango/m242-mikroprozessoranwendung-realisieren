# Implementierung

## Inhaltsverszeichnis
- [Implementierung](#implementierung)
  - [Inhaltsverszeichnis](#inhaltsverszeichnis)
    - [Sourcecode](#sourcecode)

### Sourcecode 
Hier sind alle Scripts abgelegt welche wir für unser Projekt verwendet haben. Dies sind die Codes vom Arduino selbst und der Code für die Website vom RaspberryPi.
- [Hier](https://gitlab.com/AbrissMango/m242-mikroprozessoranwendung-realisieren/-/blob/main/implementierung/arduino.ino) der Code vom Arduino.
- [Hier](https://gitlab.com/AbrissMango/m242-mikroprozessoranwendung-realisieren/-/blob/main/implementierung/arduino_secrets.h) die Credentials für das WiFi.
- [Hier](https://gitlab.com/AbrissMango/m242-mikroprozessoranwendung-realisieren/-/blob/main/implementierung/chart.html) der Code für die Website mit den Diagrammen.
- [Hier](https://gitlab.com/AbrissMango/m242-mikroprozessoranwendung-realisieren/-/blob/main/implementierung/server.js) der Code für das Backend.