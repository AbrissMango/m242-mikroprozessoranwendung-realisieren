# Einleitung allgemein
Dies die die Projektdokumentation ist für die LB3 im Modul 242 bei [Christoph Jäger](https://gitlab.com/micpre). <br>

# Inhaltsverszeichnis

## [Analyse](/analyse/)
- 1. Funktionale/ nicht Funktionale Anforderungen
- 2. Blockschaltbild
- 3. Signalbeschrieb

## [Design](/design/)
- 1. Werte-Zuweisungstabelle
- 2. Programmablauf

## [Implementierung](/implementierung/)
- 1. Source Code

## [Testing](/testing/)
- 1. Testprotokoll

## [Reflexion](/relfexion/)
- 1. Reflexion Andreas Giger
- 2. Reflexion Janik Bolli

- - -
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/ch/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/3.0/ch/88x31.png" /></a><br />Dieses Werk ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/ch/">Creative Commons Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Schweiz Lizenz</a>

- - -
