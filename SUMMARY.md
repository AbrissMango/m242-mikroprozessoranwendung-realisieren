# Zusammenfassung

## Themen
 - [Analyse](/analyse/README.md)
 - [Design](/design/README.md)
 - [Implementierung](/implementierung/README.md)
 - [Testing](/testing/README.md)
 - [Reflexion](/relfexion/README.md)