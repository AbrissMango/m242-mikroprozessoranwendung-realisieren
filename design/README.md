# Design

## Inhaltsverszeichnis
- [Design](#design)
  - [Inhaltsverszeichnis](#inhaltsverszeichnis)
    - [Arduino Anschlüsse](#arduino-anschlusse)
    - [Struktogramm](#struktogramm)

### Arduino Anschlüsse
| Pin | Hardware |
|-----|----------|
| 0 | Button |
| GND | Button |
| GND | LCD Display |
| 5V | LCD Display |
| SCL (12) | LCD Display |
| SDA (11) | LCD Display |
| * | ENV Shield |

### Struktogramm
![Struktogramm](images/struktogramm.png)
